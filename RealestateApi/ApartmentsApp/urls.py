from django.conf.urls import url
from django.urls import register_converter
from ApartmentsApp import views

from django.conf.urls.static import static

class DateConverter:
    regex = '\d{4}-\d{2}-\d{2}'

    def to_python(self, value):
        return datetime.strptime(value, '%Y-%m-%d')

    def to_url(self, value):
        return value

register_converter(DateConverter, 'yyyy')

urlpatterns=[
    url(r'^apartments$',views.apartmentsApi),
    url(r'^apartments/date/<yyyy:date>$',views.apartmentsApi),
    url(r'^apartment/([0-9]+)$',views.apartmentsApi),
    url(r'^apartment/([0-9]+)/date/<yyyy:date>$',views.apartmentsApi),
    url(r'^apartment/last$',views.lastApartmentApi),

    url(r'^projects$',views.projectsApi),
    url(r'^projects/date/<yyyy:date>$',views.projectsApi),
    url(r'^project/([0-9]+)/$',views.projectsApi),
    url(r'^project/(?P<id>([0-9]+))/(?P<date>\d{4}-\d{2}-\d{2})$',views.projectsApi,),
    url(r'^project/last$',views.lastProjectApi),

    url(r'^availability$',views.projectAvailabilitiesApi),
    url(r'^availability/(?P<id>([0-9]+))$',views.projectAvailabilitiesApi),
    url(r'^availability/(?P<id>([0-9]+))/(?P<date>\d{4}-\d{2}-\d{2})$',views.projectAvailabilitiesApi),

    url(r'^status$',views.apartmentStatusesApi),
    url(r'^status/(?P<id>([0-9]+))$',views.apartmentStatusesApi),
    url(r'^status/(?P<id>([0-9]+))/(?P<date>\d{4}-\d{2}-\d{2})$',views.apartmentStatusesApi),

]