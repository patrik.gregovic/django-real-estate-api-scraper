# Real Estate API 🏠

## Description 📝
This project consists of a web scraper which scrapes yit.sk and zelezna.sk websites for real estate data which it then stores in a SQLite database. This data is then accessible from the endpoints exposed by the Django REST API created in this project. 

The script is intended to be run daily and the database is designed with that in mind insomuch that it can keep track of historical data for each apartment and project giving us data which can be use for frontend visualization of trends.

## Getting started 🏁

### Run the scraper 🔎
Use the command `python runScrapers.py` from inside the `RealestateApi/Scraper` folder.
The scraper will make sure to fill in the database with any missing apartments and projects or update values for existing ones. The database also keeps track of the historical change in crucial values such as apartment price and availability as well as number of available apartments in a project.
```mermaid
flowchart LR
 id1(python runScrapers.py)---Scraper-->id2[(Database)]
 style id1 fill:#f0f0f0,stroke:#666,stroke-width:2px,color: #fffff,stroke-dasharray: 5 5
```
### Start the API server ⚙️
Use the command `python manage.py runserver` from inside the `RealestateApi` folder. Through the API we can reach all of the values from the database using the exposed API endpoints. This can be used to further implement a frontend which can visualize trends in price change and similar metrics.
```mermaid
flowchart LR
 id1(python manage.py runserver)---id2[Django API]-->id3[(Database)]-->id2[Django API]
 id2[Django API]-->id4[Client]-->id2[Django API]
 style id1 fill:#f0f0f0,stroke:#666,stroke-width:2px,color: #fffff,stroke-dasharray: 5 5
```

## Try out some of the API calls 💡
There are many endpoints available in this API  - considering the SQLite database is in the repository, here are some endpoints you can try with the data that's already inside:
-   **get all projects:** `GET http://127.0.0.1:8000/projects`
-   **get project availability at specific date:** `GET http://127.0.0.1:8000/availability/{id}/{yyyy-mm-dd}`
-   **get project at specific date:** `GET http://127.0.0.1:8000/availability/{id}/{yyyy-mm-dd}`
-   **get last inserted project:** `GET http://127.0.0.1:8000/project/last`
-   **get project by ID:** `GET http://127.0.0.1:8000/project/{id}`
-   **get all apartments :** `GET http://127.0.0.1:8000/apartments`
-   **get apartment status at specific date:** `GET http://127.0.0.1:8000/apartment/{id}/{yyyy-mm-dd}`
-   **get apartment at specific date:** `GET http://127.0.0.1:8000/status/{id}/{yyyy-mm-dd}`
-   **get last inserted apartment :** `GET http://127.0.0.1:8000/apartment/last`
-   **get apartment by ID:** `GET http://127.0.0.1:8000/apartment/{id}/`
